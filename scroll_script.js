window.onscroll = function() {scrollFunction()};

var navbar = document.getElementById('navbar');

function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        navbar.classList.remove('navbar--transparent');
    } else {
        navbar.classList.add('navbar--transparent');
    }
}